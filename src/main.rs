extern crate iron;
extern crate staticfile;
extern crate mount;

fn main() {
    let mut mount = mount::Mount::new();

    mount.mount("/", staticfile::Static::new(std::path::Path::new("public/")));

    let key = "PORT";

    let port: String = match std::env::var(key) {
        Ok(val) => val,
        Err(_) => String::from("8080"),
    };

    let http_url = format!("0.0.0.0:{}", port);

    iron::Iron::new(mount).http(http_url).unwrap();
}
