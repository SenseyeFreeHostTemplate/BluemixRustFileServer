# Bluemix (IBM) Rust File Server template

Rust static file server example for return `*.html` or `*.js`, `*.css`, `*.*` files

##### Local run
```bash
cargo run
```

##### Bluemix
```bash
bluemix app push rust-static-file-server
```

##### Example:
[https://rust-static-file-server.eu-gb.mybluemix.net/](https://rust-static-file-server.eu-gb.mybluemix.net/)
